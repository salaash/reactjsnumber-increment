### About the repository ###

This repo is for number increment and decrement in react

### How to set up? ###

* Git clone
* cd into application folder
* Install Dependencies by running npm install
* run 'npm start' 
* after the server starts access the application in your browser http://localhost:3000

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


